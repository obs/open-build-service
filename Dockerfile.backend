# Global ARGs shared by all stages
ARG DEBIAN_FRONTEND=noninteractive
ARG GOPATH=/usr/local/go

FROM rust:1.70-bullseye as aptly-tools-builder
RUN git clone https://github.com/collabora/aptly-rest-tools \
              -b v0.0.4 --depth=1 \
              /tmp/aptly-rest-tools
WORKDIR /tmp/aptly-rest-tools
RUN cargo build --release

FROM debian:bullseye-slim as server
LABEL maintainer Andrej Shadura <andrew.shadura@collabora.co.uk>
ENV LC_ALL=C.UTF-8
ARG WORKDIR=/tmp/sources
ARG DEBIAN_FRONTEND
ARG GOPATH

# Needs checking what’s actually needed
RUN apt-get update \
 && apt-get install -y \
        apt-utils \
        adduser \
        ca-certificates \
        curl \
        devscripts \
        diffutils \
        dpkg-dev \
        git \
        locales \
        libbssolv-perl \
        libcompress-raw-zlib-perl \
        libdatetime-format-duration-perl \
        libdatetime-format-http-perl \
        libdatetime-perl \
        libfile-sync-perl \
        libio-compress-perl \
        libjson-xs-perl \
        libnet-ssleay-perl \
        libsocket-msghdr-perl \
        libtimedate-perl \
        libxml-parser-perl \
        libxml-simple-perl \
        libxml-structured-perl \
        libyaml-libyaml-perl \
        libyaml-tiny-perl \
        make \
        patch \
        procps \
        supervisor \
        tzdata \
        xzdec \
        zstd

RUN mkdir -p /etc/apt/sources.list.d
COPY docker/apt/*.list /etc/apt/sources.list.d/
COPY docker/apt/*.gpg /usr/share/keyrings/

# reprepro #697630
# libsolv1: zstd support
RUN apt-get update \
 && apt-get install -y \
        apt-utils \
        libsolv1 \
        libsolvext1 \
        reprepro

COPY . $WORKDIR

RUN make -C $WORKDIR/dist install
RUN make -C $WORKDIR/src/backend install
RUN make -C $WORKDIR/src/backend/build install
RUN ln -sf /usr/lib/obs-build /usr/lib/obs/server/build

RUN rm -rf $WORKDIR

RUN mkdir -p /etc/obs
RUN cp /usr/lib/obs/server/BSConfig.pm.template /etc/obs/BSConfig.pm

RUN ln -sf /etc/obs/BSConfig.pm /usr/lib/obs/server/BSConfig.pm

# Sanity check: older versions of obs-build don’t have this file
RUN test -f /usr/lib/obs/server/build/Build/Modules.pm

COPY docker/services/backend/*.conf /etc/supervisor/conf.d/
COPY docker/ /opt/

RUN /opt/configure-backend-user.sh

COPY --from=aptly-tools-builder \
  /tmp/aptly-rest-tools/target/release/aptlyctl \
  /tmp/aptly-rest-tools/target/release/obs2aptly \
  /usr/local/bin/

RUN mkdir -p /srv/obs/log /srv/obs/run \
 && chmod ug=rwxt /srv/obs/run \
 && chown obsrun:obsrun -R /srv/obs

VOLUME /srv/obs

ENTRYPOINT /opt/backend-docker-entrypoint.sh

# serviceserver
EXPOSE 5152
# reposerver
EXPOSE 5252
# srcserver
EXPOSE 5352

HEALTHCHECK --start-period=60s CMD curl --fail http://127.0.0.1:5252/
